---
title: "Transmissões das palestras do Debian Day 2014 no Brasil"
kind: article
created_at: 2014-08-15 21:50
author: Paulo Santanta
---

## As palestras abaixo serão transmitidas por streaming.

## 14h00min

-   Empacotamento de Software no Debian
-   Palestrante: João Eriberto Mota Filho
-   Cidade: Trindade
-   Link: a definir

## 15h30min

-   Título: O projeto Debian precisa de você: por quê e como colaborar
-   Palestrante: Antonio Terceiro
-   Cidade: Curitiba
-   Link: a definir

## 16h30min

-   Título: Como instalar o Debian
-   Palestrante: Luiz Eduardo Guaraldo
-   Cidade: Porto Alegre
-   Link: [https://mconf.org/spaces/debian-day-2014/webconference](https://mconf.org/spaces/debian-day-2014/webconference)
