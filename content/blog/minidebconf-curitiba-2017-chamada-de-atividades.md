---
title: "MiniDebConf Curitiba 2017: chamada de atividades"
kind: article
created_at: 2016-12-19 16:43
author: Paulo Henrique de Lima Santana
---

Nos dias 17, 18 e 19 de março acontecerá a **MiniDebConf Curitiba 2017** no
Campus Central da [UTFPR - Universidade Tecnológica Federal do Paraná,](http://www.utfpr.edu.br/curitiba/o-campus/pasta2)
em Curitiba - Paraná.

Estamos recebendo propostas de palestras e oficinas para os três dias do evento,
obviamente com temas relacionados ao Debian :-)

As palestras podem ser de todos os níveis, mas tenha em mente que o público alvo
principal são os participantes que estão começando o seu contato com o Debian e
querem aprender mais.

Já as oficinas são atividades do tipo mão na massa voltado para os participantes
mais experientes que poderão contribuir em alguma área como empacotamento,
tradução, solução de bugs, etc.

As propostas de atividades deve ser enviadas pela página wiki da organização:

<https://wiki.debian.org/Brasil/Eventos/MiniDebConfCuritiba2017/Atividades/CFP>

Esperamos propostas de todos, desde usuários até Desenvolvedores oficiais
Debian.

Qualquer dúvida, pode me enviar pelo email: <terceiro@debian.org>

**Organização:**

![Minidebconf 2017 curitiba banner org](/blog/imagens/minidebconf-2017-curitiba-banner-org.png)

