---
title: "Como colaborar"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

# Como Colaborar com o Projeto Debian

Você quer começar a colaborar com o Debian? Veja abaixo onde seu perfil mais se
encaixa. Se você já é um(a) colaborador(a) e seu método ou grupo de colaboração 
não está listado nessa página, por favor compartilhe conosco!

Usar, testar, e enviar bugs para o [Sistema de Rastreamento de Bugs (BTS - Bug
Tracking System)](http://bugs.debian.org).

Enviar patches com correções para bugs.

Ajudar outros(as) usuários(as) em:

- [Listas de discussão](https://wiki.debian.org/Brasil/Listas)
- [Canais de IRC](https://wiki.debian.org/Brasil/IRC)
- [Fórum](http://www.forumdebian.com.br/) (não oficial)
- [Grupos no Telegram](/grupos-no-telegram)

Traduzir. Veja como começar [aqui](https://wiki.debian.org/Brasil/Traduzir):

- Páginas web do site oficial
- Descrições de pacotes
- Manpages
- Documentos

Divulgar o Debian nesses locais abaixo e em vários outros. Veja alguns
materiais gráfico [aqui](https://debian.pages.debian.net/debian-flyers)

- Sites
- Escolas
- Universidades/Faculdades
- Sindicatos
- Eventos como congressos, fóruns, encontros, etc.

Escrever tutoriais básicos ou avançados, para usuários(as) ou desenvolvedores(as).
A [wiki do Debian](https://wiki.debian.org/Brasil/Documentos) é um bom lugar
para publicá-los.

Fazer testes de instalação e
[enviar relatórios](https://d-i.debian.org/manual/en.i386/ch05s04.html#submit-bug).

Manter pacotes de software no Debian. Leia este
[FAQ](https://eriberto.pro.br/wiki/index.php?title=FAQ_empacotamento_Debian)
sobre empacotamento. Veja alguns pacotes que você pode contribuir:

- [Pedidos de pacotes novos](https://www.debian.org/devel/wnpp/requested)
- [Paotes órfãos](https://www.debian.org/devel/wnpp/orphaned.pt.html)
- [Pacotes mantidos por equipes](https://wiki.debian.org/Teams#Packaging_teams)

Desenvolver atividades de
[publicidade e imprensa](https://wiki.debian.org/Teams/Publicity), e
[escrever notícias](https://micronews.debian.org/pages/contribute.html).

Produzir materiais gráficos como logos, banners, wallpapers, ícones, camisas, e
publicar no <http://collab.debian.net>

Organizar [eventos e encontros](https://wiki.debian.org/Brasil/Eventos) focados
em Debian na sua cidade.