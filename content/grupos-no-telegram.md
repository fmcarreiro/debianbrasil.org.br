---
title: "Grupos no Telegram"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

# Grupos no Telegram

A comunidade brasileira mantem alguns grupos sobre Debian no telegram. Cada
grupo tem suas peculariedades, regras, formas de administrar, etc.

## Debian Brasil

- <https://t.me/debianbrasil>
- Descrição: O foco desse grupo é DEBIAN. Sobre outras distros e GNU/Linux em
geral, procure outros grupos.

## Debian Br:

- <https://t.me/debianbr>
- Descrição: Bem vindos! Grupo destinado a trocas de idéias sobre Linux. Entre
outros assuntos. Não há regras, viva o bom senso!

## Universo Debian

- <https://t.me/UniversoDebian>
- Descrição: Grupo de usuários, e entusiastas do Debian GNU/Linux com o
objetivo de estimular o desenvolvimento de um espírito de comunidade e amizade.