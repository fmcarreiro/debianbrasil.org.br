# debianbrasil.org.br

Este repositório contém os fontes do site do [Debian
Brasil](https://debianbrasil.org.br/). Ele usa [nanoc](https://nanoc.ws/), um
gerador de sites estáticos.

## Como testar o site localmente

Pacotes necessários: instale os pacotes listados no
[.gitlab-ci.yml](.gitlab-ci.yml), mais o pacote `rerun`.

Abra dois terminais:

Terminal | Comando | Função
---------|---------|-------
Terminal 1 | `rerun --no-notify --exit -- nanoc` | Recompila o site automaticamente ao salvar
Terminal 2 | `nanoc view` | Mini servidor web

Acesse sua versão local do site em <http://localhost:3000/>.

Você pode usar um terceiro terminal pra editar o arquivos, ou usar um editor
gráfico, tanto faz.

Da primeira vez, a compilação pode levar alguns segundos. A partir daí, cada
recompilação deve ser bem rápida, exceto quando você altera um arquivo que faz
com que o site todo seja modificado (ex. `layouts/default.html`).

Se você fizer uma alteração, der um reload, e a página em questão não for
atualizada, verifique o _Terminal 1_ pra ver se não tem uma mensagem de erro.
